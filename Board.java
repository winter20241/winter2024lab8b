import java.util.Random;
class Board {
	private Tile[][] grid;
	final int SIZE = 5;
	
	public Board() {
		Random rng = new Random();
		this.grid = new Tile [SIZE][SIZE];

		for(int i = 0; i<grid.length; i++) {
			for(int j = 0; j<grid.length; j++) {
				grid[i][j] = Tile.BLANK;
			}
		}
		for(int i = 0; i<grid.length; i++) {
			int j = rng.nextInt(SIZE);
			grid[i][j] = Tile.HIDDEN_WALL;
		}
	}
	
	public String toString() {
		String print = "  ";
		for(int i = 0; i<SIZE; i++) {
			print = print + i + " ";
		}
		print = print + "\n";
		for(int i = 0; i<grid.length; i++) {
			print = print + i + " ";
			for(int j = 0; j<grid[i].length; j++) {
				if(grid[i][j] == Tile.BLANK || grid[i][j] == Tile.HIDDEN_WALL) {
					print = print + "_ ";
				}else if(grid[i][j] == Tile.WALL) {
					print = print + "W ";
				}else {
					print = print + "C ";
				}
			}
			print = print + "\n";
		}
		return print;
	}
	
	public int placeToken(int row, int col) {
		if(row>4 || row<0 || col>4 || row <0) {
			return -2;
		}else if(grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
			return -1;
		}else if(grid[row][col] == Tile.HIDDEN_WALL) {
			grid[row][col] = Tile.WALL;
			return 1;
		}else {
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
}