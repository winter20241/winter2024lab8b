import java.util.Scanner;
class BoardGameApp {
	public static void main(String args []) {
		Scanner reader = new Scanner(System.in);
		Board b1 = new Board();
		System.out.println("Welcome to game");
		int numCastles = 5;
		int turns = 8;
		int round = 1;
		while(numCastles>0 && turns>0) {
			System.out.println("\nRound: " + round + "\n" + b1 + "\nNumber of castles left to find: " + numCastles + "\nEnter two numbers the first one representing the row and second representing the column of where you want to play");
			int row = reader.nextInt();
			int col = reader.nextInt();
			int num = b1.placeToken(row, col);
			while(num<0) {
				System.out.println("In vaild move, place enter where you would like to play again");
				row = reader.nextInt();
				col = reader.nextInt();
				num = b1.placeToken(row, col);
			}
			if(num == 1){
				System.out.println("\nThere was a wall");
				turns--;
			}else{
				System.out.println("\nCasle was successfully placed");
				turns--;
				numCastles--;
			}
			
			round++;
		}
		System.out.println("End of Game");
		if(numCastles == 0) {
			System.out.println("You win :)");
		}else {
			System.out.println("You lose :(");
		}
	}

}